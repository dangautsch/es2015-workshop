// Let Variables and Scope
function myFunction() {
    var x = 3;
    while (true) {
        let x = 20;
        console.log(x);
        break;
    }
    console.log(x);
}
myFunction();

// Constants
var mikey = "mikey";
mikey = "not mikey";
const DEVELOPER = mikey;

console.log(DEVELOPER);

// Classes
class Worker {
    constructor(name) {
        this.name = name;
        this.babel = "something";
    }
    sayName() {
        console.log("My name is " + this.name);
    }

    get Name() {
        return this.name;
    }

    set Name(value) {
        this.name = value;
    }

}

let employee = new Worker("Lisa");
console.log(employee.Name);
employee.Name = "My Name";
console.log(employee);


// Extending Classes

class Developer extends Worker {
    constructor() {
        super();
        this.name = "kevin";
    }
}

// Static class methods
class Food {
    constructor(type) {
        this.type = type;
    }

    static cutFood(food) {
        let cutFood = food.split(food.charAt(1));
        return cutFood;
    }

    eat() {
        let cutFood = Food.cutFood(this.type);
        console.log("I love eating " + cutFood[0]);
    }

}

let food = new Food("Broccoli");
food.eat();

// Arrow Functions
let myColors = ["Red", "Yellow", "Blue", "Green", "Apple"].filter((color) => color !== "Apple");
console.log(myColors);

// Destructuring
let a = "one";
let b = "two";
let c = {a,b};
console.log(c);
let [x, y] = [0, 1];
console.log(x, y);
